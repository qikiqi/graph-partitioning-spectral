\documentclass[]{article}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
%\usepackage[demo]{graphicx}
\usepackage{pdfpages}
\usepackage{pdflscape}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{hyperref}
\usepackage{lipsum}
\usepackage{xcolor}

\bibliographystyle{unsrt}

\graphicspath{{./img/}}
% \usepackage[printwatermark]{xwatermark}
% \newwatermark[allpages,color=lightgray!50,angle=45,scale=4,xpos=0,ypos=0]{DRAFT}

%opening
\title{Graph Partitioning with Spectral Clustering}
\author{Saska Tirronen 427719 saska.tirronen@aalto.fi, \\ Kimmo Mikkola 719249 kimmo.mikkola@aalto.fi}

\begin{document}

\maketitle

\begin{abstract}
	
Methods using eigenvectors and eigenvalues of a matrix are called spectral clustering methods \cite{meila_spectral_2016}.
These methods can be used to obtain approximate solutions on graph clustering \cite{zhang_multiway_2008}.
They have many benefits over other clustering methods and algorithms.
Thus developing and improving these methods might yield interesting insight into the vast amounts of data that we nowadays have the ability to produce and possess.
In this paper, our aim was to apply spectral clustering to the problem of graph partitioning.
The approach we ended up using is similar to the one proposed by Ng et al. (2002) \cite{ng_spectral_2001}.
We experimented with different hyper parameter setups on four graphs of different sizes, using RatioCut (Hagen and Kahng, 1992) as our objective function \cite{hagen_new_1992}.
The obtained values were 5.23 for ca-GrQc, 11.71 for Oregon-1 18.60 for soc-Epinions1 and 62.03 for web-NotreDame.
We also attempted to run the algorithm on roadNet-CA, but our computational resources were insufficient for that.
As RatioCut values can't be compared between different graphs or different amounts of clusters, the obtained scores can be seen as subjects of comparison for different algorithms on these same graphs and same amount of clusters.
We also evaluated the standard deviation of cluster sizes for each of the graphs, which gives us better idea about the evenness of the partitions.
The order of graphs from the most even to the least even partition was: web-NotreDame, Oregon-1, soc-Epinions1, ca-GrQc.
Also it should be noted that we didn't participate in the course competition.



\end{abstract}

\section{Introduction}

Unsupervised learning is widely used technique to find structure and patterns in data set without pre-existing labels.
Its popularity can be partly attributed to the fact that it needs no labeling of the data sets, as opposed to supervised learning, where costly labeling is necessary.
Unsupervised learning usually focuses on solving problems related to dimensionality reduction or clustering.
Spectral clustering is a technique where we aim to identify clusters based on the edges connecting them while drawing insight from spectral and structural properties of the graph.
The benefits of spectral clustering are its flexibility in finding clusterings in arbitrary shapes and the ability to use a similarity to cluster points instead of distance, as in the case of k-means clustering algorithms.
\cite{hinton_unsupervised_1999, meila_spectral_2016}
\\
\\
In this paper, we perform experiments with a modified algorithm based by Ng et al. (2001) \cite{ng_spectral_2001}.
We experiment its performance on various graphs from the Stanford Large Network Dataset Collection \cite{leskovec_snap_2014}.
We perform the experiments on each largest connected component from the following graphs from the Stanford Large Network Dataset Collection: ca-GrQc, Oregon-1, soc-Epinions1, web-NotreDame and roadNet-CA.
These largest connected components for each graph were provided us by course personnel of the CS-E4600 Algorithmic Methods of Data Mining course in Aalto University.
\\
\\
The experiments were performed on a regular laptop computer comprising 8GB of RAM and an Intel i7-3520M CPU with two cores and four threads running maximum of 3.6 GHz.
Our algorithm uses methods from well known Python libraries such as NumPy, SciPy and Scikit-learn \cite{walt_numpy_2011, virtanen_scipy_2019, pedregosa_scikit-learn:_2011}.
Unfortunately our experiments on the largest connected component of the graph roadNet-CA was partly incomplete due to our algorithms inefficiency on computationally limited environment described earlier.

\section{Literature Review}

Clustering algorithms are widely researched topic \cite{jain_data_2010}.
They have been researched well over half a century.
The original algorithm for k-means clustering was proposed already in 1957 although only published 1982 as a journal article \cite{lloyd_least_1982}.
Initially Lloyd did not use the name k-means of his algorithm \cite{lloyd_least_1982}.
James MacQueen coined the term k-means in 1967 \cite{macqueen_methods_1967}.
\\
\\
The research of different clustering algorithms has continued steadily over the past decades.
This is partly due clustering algorithms being an unsupervised method, therefore needing no labeling on the data that it is meant to explore.
This reduces the costs of using it when compared to supervised methods.
And this is where the usefulness of clustering algorithms lies, they need no labeling for exploring the data and finding structure in it.
\\
\\
One can learn and extract a lot of information by organizing data into groups.
Due to the advances in technology and the explosive growth in the amount of data we hold, clustering algorithms have become even more fundamental in exploiting that data.
K-means clustering aims to divide \textit{n} observations into \textit{k} clusters, in a way that the clusters comprise the observations that have the nearest mean to that cluster \cite{lloyd_least_1982}.
\\
\\
Unfortunately the aforementioned task is NP-hard, which is why there exists efficient heuristic algorithms to improve the performance of k-means clustering \cite{mahajan_planar_2012}.
By utilizing these heuristics, k-means clustering produces reasonable results fast although it has no approximation guarantees.
\\
\\
K-means clustering has some drawbacks that make it less useful under certain circumstances.
When applying it in situations where the data is greatly non-spherical or it exhibits complex cluster shapes, it can yield counterintuitive results.
This is due to it favoring to pick dense spherical clusters as opposed it picking non-spherical clusters \cite{raykov_what_2016}
Now in the case where there are complex cluster shapes in the data, it would be possible to separate these clusters by using the embedded space that the two leading eigenvectors provide.
\cite{itzhak_spectral_2005}
\\
\\
And this is exactly where spectral graph theory comes in to play.
The objective of spectral graph theory is that we can use the structral properties of the graph with spectral properties to identify connections between the two.
As we know, we can represent the graphs as matrices for example adjacency matrices or various Laplacian matrices.
Then these spectral properties can be eigenvalues and eigenvectors.
The structural properties of the graph to be identified can be connectivity, bipartiteness or cuts.
\cite{gionis_slide_2019}
\\
\\
Zhang and Jordan (2008) define spectral clustering as a `broad class of clustering procedures in which an intractable combinatorial optimization formulation of clustering is “relaxed” into a tractable eigenvector problem, and in which the relaxed solution is subsequently “rounded” into an approximate discrete solution to the original problem'.
Spectral Clustering was initially conceived in graph partitioning context, where the goal was to the partitions the graphs so that the sum of the edge weights would be minimized within the edges linking these partitions.
It was proposed by Donath and Hofman (1973) and Fiedler (1973) \cite{donath_lower_1973, fiedler_algebraic_1973}.
The aforementioned happened in 1973 and after that spectral clustering has received a fair amount of interest and research.
\cite{zhang_multiway_2008}
\\
\\
In the beginning of the 2000s spectral clustering was widely researched topic, undergoing rapid development \cite{zhang_multiway_2008}.
Many notable papers were published early 2000s \cite{zhang_multiway_2008}.
A novel approach to perceptual grouping problem based on spectral clustering was proposed by Shi and Malik (2000) in 2000 \cite{jianbo_shi_normalized_2000}.
The same year, Kannan et al.~(2000) proposed a way novel way of measuring the quality of a clustering where the quality was assessed by a variant of spectral clustering algorithm \cite{kannan_clusterings-good_2000}.
Many more notable papers were published, such as \cite{ding_equivalence_2005} and \cite{zha_spectral_2001} which all were using spectral clustering techniques.
But most importantly in 2001, Ng et al.~(2001) present in their paper a new algorithm for spectral clustering which aims to solve some of the then unresolved difficulties that spectral clustering algorithms at that time had.
Their algorithm focuses on finding reasonable clustering that can be implemented in a few lines of Matlab code. \cite{ng_spectral_2001}
This is the algorithm that we have based our algorithm presented in this paper.
\\
\\
It seems that nowadays spectral clustering doesn't receive the same amount of interest as it used to \cite{noauthor_google_2004}.
Although not as popular as it used to be, it still remains an useful tool for performing clustering on complex datasets.
This is partly due to the fact that we have perpetually growing capabilites to harvest data from which we would benefit if we'd perform clustering then drawing conclusions based on these clusterings.
Examples of these powerful data harvesting capabilites are various social media sites, systems that track collect data on people's movements, such as many Android smartphones, payment transaction information that various providers collect and many more.
Spectral clustering can also be used in the field of bioinformatics \cite{higham_spectral_2007}


\section{Algorithm and Methods}
To avoid ambiguousness, it should be noted, that in this work, we use notation of form $[a,b]$ to denote the set $\{x\in\mathbb{Z}\,\,\, |\,\, a \leq x\leq b\}$.\\
\\
So let's say we have a graph $G=(V,E)$, where $V=\{v_1,...,v_n\}$ and $E=\{e_1,...e_m\}$ are sets of vertices and edges respectively.
Our objective was to perform graph partitioning to $k$ sub-graphs such that it minimizes RatioCut objective function, which was defined by Hagen and Kahng (1992), as 
\\
\\
$$
\text{RatioCut}(A_1,...,A_k):=
\sum_{i=1}^k
\frac{\text{cut}(A_i\bar{A_i})}{|A_i|}
$$\\
\\
where $A_i$ represents the $i$:th subgraph of $G$, $\bar{A_i}$ represent its complement, and $\text{cut}(A_i,\bar{A_i})$ is the cardinality of a set of edges of G with one endpoint in $A_i$ and the other endpoint in $\bar{A_i}$.
It can be shown that the value of RatioCut function is minimized when the sizes of all sub-graphs are equal \cite{von_luxburg_tutorial_2007}.
\\
\\
One method for minimizing the RatioCut is to use normalized spectral clustering. We decided to loosely follow one such approach, suggested by Ng et al.~(2001) \cite{ng_spectral_2001}.
Below we describe the algorithm step-by-step.
\\
\\
The first thing to do is to form an affinity matrix $A\in \mathbb{R}^{n\times n}$, which, in general, captures the similarity measures of each pair of vertices in set $s=\{V\times V\}$, such that $A_{ij} = f(v_i,v_j)$, where $f:s\rightarrow \{x\in\mathbb{R}\,\,\,|\,\,x>0\}$ is a similarity function.
These similarity values are ultimately the ones we are going to base the partitioning on.
In the case of undirected and unweighted graphs, we can simply use adjacency matrix as the affinity, since the existence of an edge in between the vertices is indeed the similarity measure we want to use in the partition task.
\\
\\
The next step is to form a normalized Laplacian matrix $L$ of the graph.
This is done by first defining a diagonal matrix $D\in\mathbb{R}^{n\times n}$ such that for all $i\in[1,n] $ we have $D_{ii}=\sum_{j=1}^n A_{ij}$.
Then, we can write the Laplacian as $L=I-D^{-1/2}AD^{-1/2}$.\\
\\
Next, we find the first $k$ eigenvectors of the matrix $L$.
By "first $k$", we mean the eigenvectors corresponding to $k$ smallest eigenvalues, discarding the eigenvalue 0.
The graphs we are working with are connected, which, according to von Luxburg (2007) means that each of the normalized graph Laplacians have one eigenvalue that is equal to 0.
To compute those $k$ eigenvalues and eigenvectors efficiently, we exploited Implicitly Restarted Lanczos Method  \cite{sorensen_implicitly_1997}.
\\
\\
Then we stack the eigenvectors to be the columns of a matrix $X\in \mathbb{R}^{n\times k}$ and from that, form another matrix $Y\in \mathbb{R}^{n\times k}$ where $Y_{ij}=X_{ij}/(\sum_jX_{ij}^2)^{-1/2}$, i.e. the rows of $X$ get normalized to unit length.
Finally, we can treat each of the rows as a point in $\mathbb{R}^k$ and use K-means algorithm to perform the clustering for them.
More precisely, we get a partition vector $c=[c_i,...,c_n]$, where $c_i$ represents the cluster index of $v_i$.
\\
\\
The algorithm described above is the solution that we ended up with after experimenting with several different setups.
Most importantly, we tried different approaches for choosing the eigenvectors to use.
Different hyperparameter settings of the Implicitly Restarted Lanczos Method were tested.
Also, we tried to exclude the first eigenvector that corresponds to the eigenvalue of 0.
None of the alternative setups were yielding benefits consistently across the different graphs we were working with, so we decided to return to the approach described earlier.
\\
\\
We were able to make experiments with four different graphs of different sizes.
The smallest one of them had only 4 158 nodes, while the largest one had 325 729 nodes. Additionally, we attempted running the analysis on the largest network of 1 957 027 nodes, but found our computational resources insufficient.
We executed the graph partitioning algorithm, described above, on each of the graphs and computed the RatioCut values for each of them.
We used different values of $k$ for each individual graph.
These values were based on recommendations in the project description.

\section{Experimental Results}
Below, you can find a table that lists the RatioCut values, as well as the amount of vertices and edges in each of the 4 graphs. Also, to give us some idea how densely connected the graphs are, we computed the ratio of vertices and edges. As mentioned earlier, roadNet-CA is too large to be computed, given the computational resources available for us.
\\
\begin{center}
 \begin{tabular}{||c c c c c c||} 
 \hline
 Graph name &Vertices  & Edges & $|E|/|V|$& k& RatioCut   \\ [0.5ex]
 \hline\hline
 ca-GrQc & 4 158 & 13 428 & 3.2 & 2 & 5.23 \\ 
 \hline
 Oregon-1 & 10 670 & 22 002 & 2.1& 5 &11.71 \\ 
 \hline
 soc-Epinions1 & 75 877 & 405 739 & 5.3 & 10 & 18.60 \\ 
 \hline
 web-NotreDame & 325 729 & 1 117 563 & 3.4& 20 & 62.03  \\  
 \hline
 roadNet-CA & 1 957 027 & 2 760 388 & 1.4& 50 & - \\ [1ex]
 \hline
\end{tabular}
\end{center}
It should be noted, that these RatioCut values are only comparable against graph partition algorithms, that were run on the same graphs, using the same $k$ amount of sub-graphs.
In other words, the values don't tell much about the relative performance of two separate cases, if they either have different graphs or different values of $k$.
So in our work, the RatioCut was used mainly to optimize the algorithm itself, as described in the previous section, keeping $k$ and the graph constant.
These values above then represent the RatioCut of the best solution that we were able to come up with, i.e. the one described in previous section.
\\
\\
This means, that while we would like to find the optimal value of $k$, it's not easy, or even possible to define that based on the RatioCut values only.
The reason is that, since RatioCut performs a summation of relative cuts over the $K$ sub-graphs, it depends on the value of $k$. We could analyse the distribution of the resulting cluster sizes, to better understand how even the partitions are and then use that information to help us optimize the value of $k$. Those values are computed below, but we didn't do the further optimization of $k$ in this work, since those values were given to us.
We don't have any reason to believe that those values were not chosen according to some relevant characteristics of the graph in question, this way attempting to do the optimisation for us.
\\
\\
In the table below, you can find statistics about the partitions resulted by the algorithm.
We have listed the cardinalities of the smallest and largest clusters, as well as the means and standard deviations of the cluster cardinalities.
All cardinality values in the table are relative to the cardinality of the particular set of vertices in the graph, i.e. $|V|$. 
This is done to make the values easier to compare.
\\
\begin{center}
 \begin{tabular}{||c c c c c c c||} 
 \hline
 Graph name& k & Smallest  & Largest &Ideal& Mean& Std.   \\ [0.5ex]
 \hline\hline
 ca-GrQc & 2 &  0.012 & 0.988& 0.5 & 0.5 & 0.488 \\ 
 \hline
 Oregon-1 & 5 & 0.018 & 0.679& 0.2 & 0.2 & 0.254 \\ 
 \hline
 soc-Epinions1 & 10 & 0.0001 & 0.996& 0.1 & 0.1 & 0.299 \\ 
 \hline
 web-NotreDame & 20 & 0.004 & 0.673& 0.05& 0.05 & 0.143  \\ [1ex]
 \hline
\end{tabular}
\end{center}
The ideal cluster sizes are listed to highlight the fact that they are equal to the means of the actual cluster sizes.
So what we really care about is how close in average the values are to the mean, i.e. what is their standard deviation.
\\
\\
Based on this information, we could say that the resulting partition is more even for web-NotreDame than for the others.
For all of the graphs, it seems that the smallest clusters are really small when compared to the ideal size.
The differences seems to come from the distribution of the larger clusters.
The result seems worst for ca-GrQc, where the larger of the two clusters was almost equal to the graph itself. One should note, however, that the evenness of the partition is also graph dependent, and thus comparing them between different graphs is heavily influenced by the characteristics of a network and the suitability of $k$. Rather than comparing the performances on different graphs, these values could be more useful for optimizing the value of $k$.
\\
\\
Below you can find a set of histograms that demonstrate the distributions visually.
The vertical red lines represent the ideal cluster sizes.
From that image we can see that the graph with most even partition, soc-Epinions, has most of the clusters near the ideal value, while in the worst case, ca-GrQc, both clusters are approximately equally far from the ideal.
\\
\\
\includegraphics{img/hist.png}
When it comes to the size of a graph or the ratio between cardinality of a set of edges and cardinality of a set of vertices, there seems to be no correlation to the evenness of the resulting partition.
\\
\\
\section{Conclusions}
In this work, we presented an algorithm to graph clustering and tested its performance on four graphs.
It was based on the idea of normalized spectral clustering, and we loosely followed the approach of Ng et al.(2001).
\\
\\
The size difference between the smallest and largest graph was quite significant: the largest graph had approximately 78 times more vertices than the smallest one.
Also, the ratio of the amount of edges and the amount of vertices in the graphs was ranging from 2.1 to 5.3.
Despite these differences, we didn't find any evidence to suggest that those aspects alone would have an effect to the performance of the algorithm.
\\
\\
The primary performance measure that we used, was RatioCut objective function, that computes the weighted sum of edges that connects a node inside the cluster to a node outside the cluster.
The weights were the multiplicative inverses of the cardinalities of the clusters.
\\
\\
As the RatioCut values depend on the graph in question, as well as the value of $k$ used, we are not able to evaluate the goodness of the obtained RatioCut values, since we don't have other points of comparison than our own experiments.
After all, both the graphs and the values of $k$ were given to us by the course personnel.
All we can say about our RatioCut values, is that these are the best ones we were able to achieve with the chosen approach, and ultimately, their goodness needs to be evaluated against other approaches.
\\
\\
To get some idea of the evenness of the resulting clusterings, we compared the distribution of the sizes of the clusters.
Ideally, all clusters would be equally large, since that would minimize the RatioCut objective function \cite{von_luxburg_tutorial_2007}.
This approach suggests, that the graphs in the order from the least even partition to the most even partition is ca-GrQc, soc-Epinions1, Oregon-1 and web-NotreDame.
\\
\\
However, one should not forget the importance of the selection of $k$, when reading these results.
While the values were given to us by the course personnel, and we can assume they were chosen carefully, it remains as a potential further study topic to optimize the value of $k$ for each of the given graphs, by using the cluster size statistics in a similar manner as described above.

\bibliography{bibliography}
% Hagen, L. and Kahng, A. (1992). New spectral methods for ratio cut partitioning and clustering.
% IEEE Trans. Computer-Aided Design, 11 (9), 1074 – 1085.\\
% \\
% Ng, A., Jordan, M., and Weiss, Y. (2002). On spectral clustering: analysis and an algorithm. In
% T. Dietterich, S. Becker, and Z. Ghahramani (Eds.), Advances in Neural Information Processing
% Systems 14 (pp. 849 – 856). MIT Press.\\
% \\
% von Luxburg, U. (2007).
% A Tutorial on Spectral Clustering.
% arXiv.org. viewed 28 November 2019. https://arxiv.org/pdf/0711.0189.pdf\\
% \\
% D. Calvetti; L. Reichel; D.C. Sorensen (1994). "An Implicitly Restarted Lanczos Method for Large Symmetric Eigenvalue Problems". Electronic Transactions on Numerical Analysis. 2: 1–21.
% 
\section{Contributions}

\begin{itemize}
  \item
Kimmo Mikkola: Code, Abstract, Introduction and Literature Review.
	\item 
Saska Tirronen: Code, Abstract, Algorithm and Methods, Experimental Results and Conclusions.
	\item
The code has been an equal contribution between both team members.
\end{itemize}


\end{document}
