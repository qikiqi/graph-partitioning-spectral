import numpy as np
import scipy as sp
import networkx as nx
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import sys
import random
import os


def _generate_colors(k):
    number_of_colors = k
    colors = ["#"+''.join([random.choice('0123456789ABCDEF') for j in range(6)])
                 for i in range(number_of_colors)]
    return colors


def draw_graph(g, k, c):
    '''
    Draws an image of the graph

    INPUTS:
    g: networkx.graph
    k: number of clusters

    '''
    pos = nx.spring_layout(g)
    colors = _generate_colors(k)
    nx.drawing.nx_pylab.draw(g, pos)
    for i, cluster in enumerate(set(c)):
        nodelist = np.where(np.array(c)==cluster)[0]
        nx.draw_networkx_nodes(g,pos,
                               nodelist=nodelist,
                               node_color=colors[i],
                               node_size=500,
                           alpha=1)
    plt.draw()
    plt.show()

def init_graph(fn):
    '''
    Inits the graph bject from the text file

    INPUTS:
    fn: str filename

    OUTPUTS:
    networkx.graph
    '''
    #fn: file name
    if fn == "nofile":
        from sklearn.datasets import make_circles
        from sklearn.neighbors import kneighbors_graph

        # create the data
        X, labels = make_circles(n_samples=500, noise=0.1, factor=.2)

        # use the nearest neighbor graph as our adjacency matrix
        A = kneighbors_graph(X, n_neighbors=5).toarray()
        return nx.from_numpy_matrix(A)
    else:
        f = open(fn, "r")
        c = f.read()
        f.close() 

        g = nx.Graph()
        for v1,v2 in [row.split(" ") for row in c.split("\n")[1:] if row != '']:
            g.add_edge(int(v1),int(v2)) #Add edges based on the file
        return g

def normalized_spectral_clustering(g,k):
    '''
    Performs the clustering for the graph

    INPUTS:
    g: networkx.graph
    k: the amount of clusters

    OUTPUTS:
    np.array of size (n,), containing the class labels for the nodes of the graph
        i:th element corresponds to i:th node
    '''
    L = nx.normalized_laplacian_matrix(g)

    #w,v = np.linalg.eig(L.A)#w: eigenvalues, v:eigenvectors
    w ,v = sp.sparse.linalg.eigs(L,
                # NOTE: This is left here because 
                # we tried it, should be mentioned in paper
            k=k,#+1,
            which='SM',
            # NOTE: This is disabled because it adds unnecessary complexity
            #ncv=10*k,
            mode='normal')
    # NOTE: This is left here because we tried it, should be mentioned in paper
    #w,v = w[1:], v[:,1:]


    # TODO: There is also the option to drop the first eigenvector + value and 
    #v = v[:,np.argsort(w)[:k]] #Picks only the eigenvectors that correspond to the largest eigenvalues
    


    # #Normalize the rows of v
    v = v / np.linalg.norm(v,axis=1).reshape(v.shape[0],1)

    # Change complex to real
    v = np.real(v)

    # #KMeans for the rows
    kmeans = KMeans(n_clusters = k)

    clust = kmeans.fit_predict(v) 

    return clust

def save_clustering_results(clust,fn,k):
    '''
    Writes the results of clustering to a .txt file

    INPUTS:
    clust: np.array of size(n,) containing the results of clustering
    fn: file name of the graph file
    '''
    f = open("../results/"+fn.split('.')[0]+"_k"+str(k)+"_clustering.txt", "w")
    n = clust.shape[0]
    for i in range(n):
        f.write(f"{i} {clust[i]}\n")

    f.close()

def save_cost(cost,fn,k):
    '''
    Writes the cost to a file

    INPUTS:
    cost: float
    fn: the file name of the graph file
    '''
    f = open(f"../results/{fn.split('.')[0]}_k{k}_score.txt", "w")

    f.write(str(cost))
    f.close()


def objective_function(clust,g):
    '''
    Computes the score for cost function

    INPUTS:
    clust: np.array of size(n,) containing the results of clustering
    g: nx.graph
    '''

    k = np.unique(clust)
    edges = g.edges()
    
    cost = 0
    for i in k: #Loops through all clusters
        in_clust = np.where(clust==i)[0]
        out_clust = np.where(clust!=i)[0]
        
        #Computes the amoun of edges that cross the cluster bounds
        n_crossing_edges = 0
        for e in edges:
            if (e[0] in in_clust and e[1] in out_clust) or  e[1] in in_clust and e[0] in out_clust:
                n_crossing_edges += 1

        #Computes the ratio and adds to total
        cost += n_crossing_edges / len(in_clust)
    return cost
        




#DATA PATH
data_path = "../data/"

#INPUT PARAMETERS
if len(sys.argv)!=3:
    print("Invalid length of input params. Using default instead.")
    fn = "dummy_graph.txt"
    k = 2
else:
    fn = sys.argv[1]
    k = int(sys.argv[2])

f_list = os.listdir(data_path) if fn=="all" else [fn]

for fn in f_list:
    print(f"Processing graph in file {fn}")
    f_path = fn if fn=="nofile" else data_path+fn
    g = init_graph(f_path)
    

    clust = normalized_spectral_clustering(g,k)
    cost = objective_function(clust,g)
    save_clustering_results(clust,fn,k)
    save_cost(cost,fn,k)
    print("--- RESULTS CALCULATED AND SAVED ----")
    # draw_graph_question = input("type OK to draw graph")
    # if draw_graph_question is not None:
    #     if draw_graph_question == "OK":
    #         draw_graph(g, k, clust)
