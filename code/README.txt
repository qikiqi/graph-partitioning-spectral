The algorithm can be run by executing main.py inside this folder. It is done by running

python main.py <filename> <n-clusters>

n-clusters parameter is used to define the value of k.

You also need to pass an argument that defines what is the graph file 
that you want to process. It suffices to pass the file name,
for example Oregon-1.txt. The graph files are assumed to be located in data folder.

You can use special filename parameter "all", to run the algorithm on 
every graph in the data folder, with the fixed value of k.
Notice, that the data folder contains more than just the
five graphs discussed in the report.

When the code is run, it saves the ratio cut scores in results folder, by name 
<graph-name>_k<n-clusters>_score.txt

and the partitioning itself, by name
<graph-name>_k<n-clusters>_clustering.txt

